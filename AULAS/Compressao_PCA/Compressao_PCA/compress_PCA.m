% Compressing an image with PCA
% 
% Last modification: 28/07/2009
% Author: Guilherme Barreto

clear; clc; close all

% Le a imagem (imread) e converte (im2double) para double precision
A=imread('lena.jpg');
An=A; %An=imnoise(A,'gaussian');
B=im2double(An);
X=im2col(B,[8 8],'distinct');

%%%%%%%% PCA %%%%%%%%%%%
Cx=cov(X');  % matriz de covariancia
[V L]=eig(Cx);  
L=diag(L);  % Vetor de autovalores
[L I]=sort(L,'descend');  % Autovalores em ordem decrescente
V=V(:,I);  % Autovetores ordenados do maior para menor autovalor

%figure; bar(L);  % grafico da amplitude dos autovalores
SL=sum(L);  % Soma dos autovalores
aux=0;
for i=1:length(L),
    aux=aux+L(i);
    VE(i)=aux/SL;   % Variancia explicada
end
figure; plot(VE); grid

% escolha dos Q maiores autovalores (componentes principais)
tol=1;  % tolerancia para VE aceitavel 
q=length(find(VE<=tol));

% Matriz de transformacao resultante
Vq=V(:,1:q);

Q=Vq';

% Imagem transformada com blocos de tamanho K
Y=Q*X;

% Imagem reconstruida (com perdas q < length(L))
Xrec=Q'*Y;

Ar = col2im(Xrec,[8 8],[256 256],'distinct');

figure; imshow(An)
figure; imshow(Ar)

