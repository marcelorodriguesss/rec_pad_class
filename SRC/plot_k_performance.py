#!/usr/bin/env python3.6

# 4

import numpy as np
import matplotlib.pyplot as plt

from os.path import dirname, realpath

work_dir = dirname(realpath(__file__))

methods = ['nn', 'cq']

for m in methods:

    k128x120 = np.loadtxt(f'{work_dir}/resultados/accuracys_k_{m}_128x120.txt')
    k50x50 = np.loadtxt(f'{work_dir}/resultados/accuracys_k_{m}_50x50.txt')
    k40x40 = np.loadtxt(f'{work_dir}/resultados/accuracys_k_{m}_40x40.txt')
    k30x30 = np.loadtxt(f'{work_dir}/resultados/accuracys_k_{m}_30x30.txt')
    k20x20 = np.loadtxt(f'{work_dir}/resultados/accuracys_k_{m}_20x20.txt')

    plt.plot(k128x120, label='128x120')
    plt.plot(k50x50, label='50x50')
    plt.plot(k40x40, label='40x40')
    plt.plot(k30x30, label='30x30')
    plt.plot(k20x20, label='20x20')
    plt.title(f'{m}'.upper())
    plt.xlabel('K')
    plt.ylabel('ACURÁCIA')
    plt.legend()
    plt.xticks(np.arange(20), np.arange(1, 21))
    # plt.yticks(np.arange(0.0, 1.0), np.arange(0.0, 1.0))
    # plt.grid()
    plt.tight_layout()
    # plt.show()
    plt.savefig(f'{work_dir}/figs/{m}_k_x_acuracia.png')
    plt.close()
