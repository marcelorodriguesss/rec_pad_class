#!/usr/bin/env python3.6

import numpy as np

# substituído g por 1 e b por -1
iono_file = '../DADOS/IONOSPHERE/ionosphere.data'

dados = np.loadtxt(iono_file, delimiter=',')

# retirando classes
X = dados[:, :-1]
print(f'shape dados sem classes: {X.shape}')

def calc_cov(X):
    meanX = np.mean(X, axis = 0)
    lenX = X.shape[0]
    X = X - meanX
    covariance = X.T.dot(X) / (lenX -1)
    return covariance

# 1
# média das linhas
cov = calc_cov(X)

print(cov[0, :])
print(cov[1, :])
print(cov.shape)

r = np.cov(X.T)
print(r[0, :])
print(r[1, :])
print(r.shape)
