#!/usr/bin/env python3.6

# 1

import os
import time
import numpy as np
from PIL import Image
# import matplotlib.pyplot as plt


def readpgm(resize_img=(128, 120)):

    # (128, 120) é o padrão das imagens

    ini_time = time.time()

    print('Lendo imagens...')

    names = ['an2i', 'at33', 'boland', 'bpm', 'ch4f', 'cheyer', 'choon',
             'danieln', 'glickman', 'karyadi', 'kawamura', 'kk49',
             'megak', 'mitchell', 'night', 'phoebe', 'saavik', 'steffi',
             'sz24', 'tammo']

    face_sides = ['left', 'right', 'straight', 'up']

    expressions = ['angry', 'happy', 'neutral', 'sad']

    eyes = ['open', 'sunglasses']

    input_dir = '/home/rodrigues/gitlab/rec_pad_class/DADOS/faces'

    # usado para armazenar e salvar todas as classes em um único arquivo
    # +1 é destinado para alocar os valores dos labels
    feat_size = len(names) * len(face_sides) * len(expressions) * len(eyes)
    class_size = resize_img[0] * resize_img[1] + 1
    X = np.full((feat_size, class_size), np.nan)

    i = 0

    for label, iname in enumerate(names):

        # ii = 0

        # usado para armazenar e salvar cada classe
        # em arquivos separadamente
        # feat_size = len(face_sides) * len(expressions) * len(eyes)
        # Xclass = np.full((feat_size, class_size), np.nan)

        for iface in face_sides:

            for iexp in expressions:

                for ieye in eyes:

                    img_name = f'{input_dir}/{iname}_{iface}_{iexp}_{ieye}.pgm'

                    # ref: https://stackoverflow.com/a/47585835
                    with open(img_name) as f:
                        lines = f.readlines()

                    # Ignores commented lines
                    for l in list(lines):
                        if l[0] == '#':
                            lines.remove(l)

                    # Makes sure it is ASCII format (P2)
                    assert lines[0].strip() == 'P2'

                    # Converts data to a list of integers
                    data = []
                    for line in lines[1:]:
                        data.extend([int(c) for c in line.split()])

                    # img_raw = (np.array(data[3:]), (data[1], data[0]), data[2])
                    # ref: https://stackoverflow.com/a/27623335
                    img_data = np.array(data[3:], dtype='uint8')
                    img_shape = (data[1], data[0])

                    # plt.imshow(img_data.reshape(img_shape))
                    # plt.show()

                    if resize_img != (128, 120):
                        img_data = np.reshape(img_data, img_shape)
                        # img_data = np.array(Image.fromarray(img_data))
                        img_data = np.array(Image.fromarray(img_data).resize(resize_img, Image.BICUBIC))
                        img_data = img_data.reshape(resize_img[0] * resize_img[1])

                    # plt.imshow(img_data.reshape(resize_img))
                    # plt.show()

                    # Xclass[ii, :] = img_data

                    img_data = np.append(img_data, label)

                    X[i, :] = img_data

                    i += 1

                    # ii += 1

        # output_file = f'output_faces/{iname}_{resize_img[0]}x{resize_img[1]}.txt'
        # print(f'Salvando: {output_file}')
        # np.savetxt(output_file, Xclass, fmt='%2.1f ', delimiter='\t')

    output_file = f'output_faces/X_{resize_img[0]}x{resize_img[1]}.txt'
    print(f'Salvando: {output_file}')
    np.savetxt(output_file, X, fmt='%2.1f ', delimiter='\t')

    print(f'time_elapsed: {time.time() - ini_time}s')


if __name__ == '__main__':

    # os.chdir('/Users/rodrigues/gitlab/rec_pad_class/SRC')  # on Mac
    os.chdir('/home/rodrigues/gitlab/rec_pad_class/SRC')  # on Linux

    # readpgm()  # resize_img=(128, 120)
    # readpgm(resize_img=(50, 50))
    # readpgm(resize_img=(40, 40))
    # readpgm(resize_img=(30, 30))
    readpgm(resize_img=(20, 20))

