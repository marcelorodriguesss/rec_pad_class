#!/usr/bin/env python3.6

# 5

import numpy as np
import matplotlib.pyplot as plt

from os.path import dirname, realpath

work_dir = dirname(realpath(__file__))

methods = ['nn', 'dmc', 'cq', 'lmq']

for m in methods:

    acc_20  = np.loadtxt(f'{work_dir}/resultados/accuracys_20x20_{m}.txt')
    acc_30  = np.loadtxt(f'{work_dir}/resultados/accuracys_30x30_{m}.txt')
    acc_40  = np.loadtxt(f'{work_dir}/resultados/accuracys_40x40_{m}.txt')
    acc_50  = np.loadtxt(f'{work_dir}/resultados/accuracys_50x50_{m}.txt')
    acc_128 = np.loadtxt(f'{work_dir}/resultados/accuracys_128x120_{m}.txt')

    fig = plt.figure(1, figsize=(9, 7))

    ax = fig.add_subplot(111)

    # ref: https://matplotlib.org/3.1.1/users/dflt_style_changes.html
    #      https://matplotlib.org/3.1.1/tutorials/introductory/customizing.html

    whisker_props = dict(color="b", linewidth=2.0, linestyle='--')
    box_props = dict(color="b", linewidth=2.0)
    median_props = dict(color="r", linewidth=2.0)
    flier_props = dict(color="b", markeredgecolor="b", markerfacecolor='b',
                       markeredgewidth='1.5', marker="+", markersize=15)
    cap_props = dict(color='b', linewidth=2.0)

    bp = ax.boxplot([acc_20, acc_30, acc_40, acc_50, acc_128],
                    whiskerprops=whisker_props,
                    boxprops=box_props,
                    flierprops=flier_props,
                    medianprops=median_props,
                    capprops=cap_props)

    ax.set_xlabel('\nTAMANHO IMAGEM', fontsize=14, fontweight='bold')

    ax.set_ylabel('ACURÁCIA\n', fontsize=14, fontweight='bold')

    ax.set_title(f'CLASSIFICADOR: {m}'.upper(), fontsize=14, fontweight='bold')

    # ax.axhline(y=lim_lower, linewidth=1.5, color='gray', ls='dashed')

    # ax.axhline(y=lim_up, linewidth=1.5, color='gray', ls='dashed')

    # ax.legend(bp['ECHAM4.6 MODEL'], loc='upper right')

    ax.set_xticklabels(['20x20', '30x30', '40x40', '50x50', '128x120'])

    # ax.set_yticklabels(np.arange(0.9, 1.0), fontweight='bold')

    ax.tick_params(labelsize=15)

    fig.savefig(f'{work_dir}/figs/boxplot_{m}.png', bbox_inches='tight', dpi=100)

    # plt.show()

    plt.close()
