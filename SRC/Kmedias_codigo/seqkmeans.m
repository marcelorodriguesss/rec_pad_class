% Implementacao do algoritmo K-medias sequencial
%
% Autor: Guilherme Barreto
% Data: 20/11/2018
%
clear; clc; close all;

X=load('dataset1.dat'); % Leitura do conjunto de dados
[N M]=size(X);

K=4;  % Numero de prototipos escolhido

% Posicao inicial dos prototipos
I=randperm(N,K);
% I=[26, 89, 80, 115]; % para testes

W=X(I,:);   % Seleciona aleatoriamente K exemplos do conjunto de dados
%W=zeros(K,2);  % Inicia prototipos com zero.

% figure;
% plot(X(:,1),X(:,2),'ro',W(:,1),W(:,2),'cd')

Ne=20;   % Numero de epocas de treinamento
lr=0.1;  % Passo de aprendizagem

for r=1:Ne,

  % Embaralha conjunto de dados a cada epoca de treinamento
  I = randperm(N);

%   I = [26, 17, 75, 71, 3, 125, 96, 22, 98, 55, 92, 80, 25, 42, 87, 102, 94, 84, ...
%         7, 88, 66, 91, 89, 77, 73, 117, 78, 103, 13, 74, 111, 43, 120, 19, 5, 113, ...
%        90, 33, 104, 6, 59, 1, 107, 97, 76, 54, 56, 122, 28, 4, 93, 108, 49, 9, ...
%        45, 124, 57, 65, 44, 106, 79, 52, 41, 27, 39, 40, 121, 34, 115, 61, 60, 62, ...
%        48, 18, 109, 50, 10, 119, 20, 2, 58, 24, 86, 99, 95, 72, 100, 15, 116, 46, ...
%        30, 47, 81, 69, 101, 82, 14, 8, 35, 64, 83, 32, 31, 53, 11, 63, 37, 51, ...
%       114, 23, 70, 36, 67, 85, 123, 68, 12, 21, 110, 105, 118, 29, 38, 16, 112]; % para testes

  X=X(I,:);

  % Busca pelo prototipo mais proximo do vetor de atributos atual
  soma=0;
  for t=1:N,
    for i=1:K,
      Dist2(i)=norm(X(t,:)-W(i,:));
    end
    [dummy Istar]=min(Dist2);   % Indice do prototipo mais proximo

    aux=X(t,:)-W(Istar,:);
    soma=soma+aux*aux';     % Usado no calculo do SSD por epoca

    % Atualiza posicao do prototipo mais proximo
    W(Istar,:) = W(Istar,:) + lr*aux;
  end

  SSD(r)=soma;

end

SSD

% Mostra posicao final dos prototipos
figure;
plot(X(:,1),X(:,2),'ro',W(:,1),W(:,2),'b*')

%plot(W(:,1),W(:,2),'gd');

% Mostra evolucao do SSD ao longo das epocas de treinamento
figure; plot(SSD); xlabel('Epocas de treinamento');
ylabel('SSD'); title('SSD por epoca de treinamento')

% Realiza particao pos-treinamento do conjunto original em K subconjuntos
for t=1:N,
    for i=1:K,
      Dist2(i)=norm(X(t,:)-W(i,:));
    end
    [dummy Istar]=min(Dist2);   % Indice do prototipo mais proximo

    Icluster(t)=Istar;
end

for k=1:K,
    I=find(Icluster==k); % Indice de todos os exemplos mais proximos do prototipo W_k
    Particao{k}=X(I,:);  % Separa todos os exemplos da k-esima particao
end




