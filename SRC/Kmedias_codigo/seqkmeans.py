#!/usr/bin/env python3.6

'''
12 Dez 2019
Algoritmo kmean sequencial transcrito do código
octave do Prof. Guilherme Barreto
'''

print(__doc__)

import numpy as np

import matplotlib.pyplot as plt

X = np.loadtxt('dataset1.dat')

N, M = X.shape

K = 4

I = np.random.choice(np.random.permutation(N), size=K)
# I = [25, 88, 79, 114];  # para testes

W = X[I, :]

plt.figure()
plt.plot(X[:, 0], X[:, 1], 'rx', W[:, 0], W[:, 1], 'cd')

Ne = 20
lr = 0.1
SSD = np.full(Ne, np.nan)

for r in range(Ne):

    I = np.random.permutation(N)

    # para testes
    # I = np.array([26, 17, 75, 71, 3, 125, 96, 22, 98, 55, 92, 80, 25, 42, 87, 102, 94, 84,
    #   7, 88, 66, 91, 89, 77, 73, 117, 78, 103, 13, 74, 111, 43, 120, 19, 5, 113,
    #    90, 33, 104, 6, 59, 1, 107, 97, 76, 54, 56, 122, 28, 4, 93, 108, 49, 9,
    #    45, 124, 57, 65, 44, 106, 79, 52, 41, 27, 39, 40, 121, 34, 115, 61, 60, 62,
    #    48, 18, 109, 50, 10, 119, 20, 2, 58, 24, 86, 99, 95, 72, 100, 15, 116, 46,
    #    30, 47, 81, 69, 101, 82, 14, 8, 35, 64, 83, 32, 31, 53, 11, 63, 37, 51,
    #   114, 23, 70, 36, 67, 85, 123, 68, 12, 21, 110, 105, 118, 29, 38, 16, 112])
    # I = I - 1

    X = X[I, :]

    soma = 0
    for t in range(N):
        Dist2 = np.full(K, np.nan)
        for k in range(K):
            Dist2[k] = np.linalg.norm((X[t, :] - W[k, :]))
        Istar = np.argmin(Dist2)
        aux = X[t, :] - W[Istar, :]
        soma = soma + aux.dot(aux.T)
        W[Istar, :] = W[Istar, :] + lr * aux

    SSD[r] = soma

plt.figure()
plt.plot(X[:, 0], X[:, 1], 'rx', W[:, 0], W[:, 1], 'b*')

plt.figure()
plt.plot(range(Ne), SSD)
print(SSD)

plt.show()

plt.close()
