#!/usr/bin/env python3.6

'''
12 Dez 2019
Algoritmo kmean batch transcrito do código
octave do Prof. Guilherme Barreto
'''

print(__doc__)

import numpy as np

import matplotlib.pyplot as plt

X = np.loadtxt('dataset1.dat')

N, M = X.shape

K = 4

I = np.random.choice(np.random.permutation(N), size=K)

# I = [25, 88, 79, 114]  # para teste

W = X[I, :]

plt.figure()
plt.plot(X[:, 0], X[:, 1], 'rx', W[:, 0], W[:, 1], 'cd')

Ne = 10

SSD = np.full(Ne, np.nan)

for r in range(Ne):

    Dmin = np.full(N, np.nan)
    Icluster = np.full(N, np.nan)

    for t in range(N):
        Dist2 = np.full(K, np.nan)
        for k in range(K):
            Dist2[k] = np.linalg.norm((X[t, :] - W[k, :]))
        Dmin[t] = np.min(Dist2)
        Icluster[t] = np.argmin(Dist2)

    SSD[r] = np.sum(np.power(Dmin, 2))

    for k in range(K):
        I = np.where(Icluster == k)
        W[k, :] = np.mean(X[I, :], axis=1)

    Icluster = np.full(N, np.nan)

    for t in range(N):
        Dist2 = np.full(K, np.nan)
        for k in range(K):
            Dist2[k] = np.linalg.norm((X[t, :] - W[k, :]))
        Icluster[t] = np.argmin(Dist2)

plt.figure()
plt.plot(X[:, 0], X[:, 1], 'rx', W[:, 0], W[:, 1], 'b*')

plt.figure()
plt.plot(range(Ne), SSD)
print('+++ SSD +++')
print(SSD)

plt.show()

plt.close()
