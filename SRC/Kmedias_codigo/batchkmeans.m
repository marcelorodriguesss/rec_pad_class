% Implementacao do algoritmo K-medias batch
%
% Autor: Guilherme Barreto
% Data: 21/11/2019
%
clear; clc; close all;

X=load('dataset1.dat'); % Leitura do conjunto de dados
[N M]=size(X);

K=4;  % Numero de prototipos escolhido

% Posicao inicial dos prototipos
I=randperm(N,K);
% I=[26, 89, 80, 115];  % para testes

W=X(I,:);   % Seleciona aleatoriamente K exemplos do conjunto de dados
%W=zeros(K,2);  % Inicia prototipos com zero.

figure;
plot(X(:,1),X(:,2),'ro',W(:,1),W(:,2),'cd')

Ne=10;   % Numero de epocas de treinamento

for r=1:Ne,

  % Busca pelo prototipo mais proximo do vetor de atributos
  for t=1:N,
      for k=1:K,
          Dist2(k)=norm(X(t,:)-W(k,:));
      end
      [Dmin(t) Icluster(t)]=min(Dist2);   % Indice do prototipo mais proximo
  end

  SSD(r)=sum(Dmin.^2);     % Soma das distancias quadraticas por iteracao

  % Particiona dados em K subconjuntos e atualiza prototipo correspondente
  for k=1:K,
    I=find(Icluster==k); % Indice de todos os exemplos mais proximos do prototipo W_k
    Particao{k}=X(I,:);  % Separa todos os exemplos da k-esima particao
    W(k,:) = mean(Particao{k});     % Atualiza posicoes dos prototipos
  end

  % Calcula SSD
  for t=1:N,
      for k=1:K,
          Dist2(k)=norm(X(t,:)-W(k,:));
      end
      [dummy Icluster(t)]=min(Dist2);   % Indice do prototipo mais proximo
  end

end

SSD

% Mostra posicao final dos prototipos
figure;
plot(X(:,1),X(:,2),'ro',W(:,1),W(:,2),'b*')

%plot(W(:,1),W(:,2),'gd');

% Mostra evolucao do SSD ao longo das epocas de treinamento
figure; plot(SSD); xlabel('Epocas de treinamento');
ylabel('SSD'); title('SSD por epoca de treinamento')
