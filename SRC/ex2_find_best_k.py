#!/usr/bin/env python3.6

# 2

import os
import time
import numpy as np
from sklearn import metrics
from sklearn.neighbors import NearestCentroid
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
# import matplotlib.pyplot as plt

# def clf(X, y, k, test_run, fig_size, clf_method='minkowski'):
def clf(X, y, k, clf_method='minkowski'):

    ini_time = time.time()

    # print(f'({clf_method}) Separando treinos e testes...')

    X_train, X_test, y_train, y_test = \
        train_test_split(X, y, test_size = 0.2)

    # print(f'({clf_method}) Configurando parametros...')

    if clf_method == 'nn':  # Vizinho mais próximo (nn): Euclidean

        clf = KNeighborsClassifier(n_neighbors=k,
                                   metric='euclidean',
                                   algorithm='brute',
                                   n_jobs=-1)

    elif clf_method == 'dmc':  # Dist. mín. centróide (dmc): Nearest Centroid

        clf = NearestCentroid(shrink_threshold=k)

    elif clf_method == 'cq':  # Classificador Quadrático (cq): Mahalanobis

        cov = np.cov(X_train.T, bias = True)
        inv_cov = np.linalg.inv(cov)

        clf = KNeighborsClassifier(n_neighbors=k,
                                   metric='mahalanobis',
                                   algorithm='brute',
                                   n_jobs=-1,
                                   metric_params={'VI': inv_cov})

    elif clf_method == 'lmq':  # linear de mínimos quadrados

        clf = LogisticRegression(solver='newton-cg',
                                 multi_class='multinomial')

    elif clf_method == 'minkowski':  # este método não é cobrado no TC2

        clf = KNeighborsClassifier(n_neighbors=k,
                                   metric='minkowski',
                                   p=2,
                                   algorithm='brute',
                                   n_jobs=-1)
    else:

        raise SystemExit('*** Check parametro: clf_method ***')

    # Train the model using the training sets
    # print(f'({clf_method}) Treinamento...')
    clf.fit(X_train, y_train)

    # Predict the response for test dataset
    # print(f'({clf_method}) Previsao...')
    y_pred = clf.predict(X_test)

    acuracy = metrics.accuracy_score(y_test, y_pred)

    # print(f'({clf_method}) Acuracia:', acuracy)

    end_time = time.time()
    elapsed_time = end_time - ini_time
    hours, rem = divmod(elapsed_time, 3600)
    minutes, seconds = divmod(rem, 60)

    str_elapsed_time = f'({clf_method}) ' \
        f'{int(hours):0>2}:{int(minutes):0>2}:{seconds:05.2f} ' \
        f'({elapsed_time}s)'

    print(str_elapsed_time)

    return acuracy


if __name__ == '__main__':

    # os.chdir('/Users/rodrigues/gitlab/rec_pad_class/SRC')  # on Mac
    os.chdir('/home/rodrigues/gitlab/rec_pad_class/SRC')  # on Linux

    # img_sizes = ['20x20', '30x30', '40x40', '50x50', '128x120']

    # encontrar melhor k

    for size in img_sizes:

        X_data = np.loadtxt(f'output_faces/X_{size}.txt')
        X = X_data[:, :-1]
        y = X_data[:, -1]

        #TODO: testar valor de k
        #TODO: Acumular acuracias
        #TODO: Boxplot acuracias

        accuracys = np.full((7, 1), np.nan)
        # accuracys = np.full((20, 1), np.nan)

        # dmc começar k com None, 0.2, ...

        for k in range(7):
            print(f' ---- k: {k+1} ({size}) ----')
            # accuracy_nn = []
            # accuracy_dmc = []
            accuracy_cq = []
            for t in range(10):  # 10 rodadas testes só para encontrar k
                # accuracy_nn.append(clf(X, y, k=k+1, clf_method='nn'))
                # accuracy_dmc.append(clf(X, y, k=k+1, clf_method='dmc'))
                accuracy_cq.append(clf(X, y, k=k+1, clf_method='cq'))
            # accuracys[k, 0] = np.mean(accuracy_nn)
            # accuracys[k, 0] = np.mean(accuracy_dmc)
            accuracys[k, 0] = np.mean(accuracy_cq)

        np.savetxt(f'accuracys_k_cq_{size}.txt', accuracys, fmt='%2.5f')

