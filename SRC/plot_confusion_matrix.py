#!/usr/bin/env python3.6

# 6

import numpy as np
import matplotlib.pyplot as plt

from os.path import dirname, realpath

work_dir = dirname(realpath(__file__))


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.YlOrRd):

    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = f'Matriz de Confusão Normalizada - {title}'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    # cm = confusion_matrix(y_true, y_pred)

    # Only use the labels that appear in the data
    # classes = classes[unique_labels(y_true, y_pred)]

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print(f'Matriz de Confusão Normalizada - {title}')
    else:
        print('Confusion matrix, without normalization')

    # print(cm)

    fig, ax = plt.subplots(figsize=(10, 10))
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)

    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes,
           yticklabels=classes,
           title=title,
           ylabel=' ',
           xlabel=' ')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")

    fig.tight_layout()

    return ax


for clf in ['nn', 'dmc', 'cq', 'lmq']:

    for img_size in ['20x20', '30x30', '40x40', '50x50', '128x120']:

        cm = np.loadtxt(f'{work_dir}/resultados/{clf}_attr_values_{img_size}.txt')

        # print(cm.shape)

        classes = ['an2i', 'at33', 'boland', 'bpm', 'ch4f', 'cheyer', 'choon',
                   'danieln', 'glickman', 'karyadi', 'kawamura', 'kk49', 'megak',
                   'mitchell', 'night', 'phoebe', 'saavik', 'steffi', 'sz24', 'tammo']

        plot_confusion_matrix(cm, classes,
                              normalize=True,
                              title=f'{clf.upper()} - {img_size}',
                              cmap=plt.cm.magma_r)

        plt.tight_layout()

        # plt.show()

        plt.savefig(f'{work_dir}/figs/matriz_confusao_{clf}_{img_size}.png')

        plt.close()
