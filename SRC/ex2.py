#!/usr/bin/env python3.6

# 3

import os
import time
import numpy as np
from sklearn import metrics
from sklearn.neighbors import NearestCentroid
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
# import matplotlib.pyplot as plt


def clf(X, y, k=1, clf_method='minkowski'):

    ini_time = time.time()

    # print(f'({clf_method}) Separando treinos e testes...')

    X_train, X_test, y_train, y_test = \
        train_test_split(X, y, test_size = 0.2)

    # print(f'({clf_method}) Configurando parametros...')

    if clf_method == 'nn':  # Vizinho mais próximo (nn): Euclidean

        clf = KNeighborsClassifier(n_neighbors=k,
                                   metric='euclidean',
                                   algorithm='brute',
                                   n_jobs=-1)

    elif clf_method == 'dmc':  # Dist. mín. centróide (dmc): Nearest Centroid

        clf = NearestCentroid(shrink_threshold=k)

    elif clf_method == 'cq':  # Classificador Quadrático (cq): Mahalanobis

        cov = np.cov(X_train.T, bias = True)
        inv_cov = np.linalg.inv(cov)

        clf = KNeighborsClassifier(n_neighbors=k,
                                   metric='mahalanobis',
                                   algorithm='brute',
                                   n_jobs=-1,
                                   metric_params={'VI': inv_cov})

    elif clf_method == 'lmq':  # linear de mínimos quadrados

        clf = LogisticRegression(solver='newton-cg',
                                 multi_class='multinomial')

    elif clf_method == 'minkowski':  # este método não é cobrado no TC2

        clf = KNeighborsClassifier(n_neighbors=k,
                                   metric='minkowski',
                                   p=2,
                                   algorithm='brute',
                                   n_jobs=-1)

    else:

        raise SystemExit('*** Check parametro: clf_method ***')

    # Train the model using the training sets
    # print(f'({clf_method}) Treinamento...')
    clf.fit(X_train, y_train)

    # Predict the response for test dataset
    # print(f'({clf_method}) Previsao...')
    y_pred = clf.predict(X_test)

    # d, b, c, a = metrics.confusion_matrix(y_test, y_pred).ravel()
    attr = metrics.confusion_matrix(y_test, y_pred)

    print(attr.shape)
    print(attr)

    acuracy = metrics.accuracy_score(y_test, y_pred)

    # print(f'({clf_method}) Acuracia:', acuracy)

    end_time = time.time()
    elapsed_time = end_time - ini_time
    hours, rem = divmod(elapsed_time, 3600)
    minutes, seconds = divmod(rem, 60)

    str_elapsed_time = f'({clf_method}) ' \
        f'{int(hours):0>2}:{int(minutes):0>2}:{seconds:05.2f} ' \
        f'({elapsed_time}s)'

    print(str_elapsed_time, acuracy)

    return acuracy, attr


if __name__ == '__main__':

    # os.chdir('/Users/rodrigues/gitlab/rec_pad_class/SRC')  # on Mac
    os.chdir('/home/rodrigues/gitlab/rec_pad_class/SRC')  # on Linux

    # methods = ['nn', 'dmc', 'lmq', 'cq']
    methods = ['cq']

    img_sizes = ['20x20', '30x30', '40x40', '50x50', '128x120']

    for m in methods:

        for size in img_sizes:

            # print(f'\n---- {size} ----')

            X_data = np.loadtxt(f'output_faces/X_{size}.txt')
            X = X_data[:, :-1]
            y = X_data[:, -1]

            n_run_tests = 10  # numero de rodadas teste

            accuracys_resume = np.full((1, 5), np.nan)  # para média, mediana, ...

            attr_values = np.full((n_run_tests, 20, 20), np.nan)  # para tabela de atributos

            # accuracys para todas as acurácias..., fazer boxplot, 1 modelos, 5 resultados
            accuracys, lst_a, lst_b, lst_c, lst_d = [], [], [], [], []

            for t in range(n_run_tests):  # rodadas testes

                print(f'+++ {m} - {size} - {t} +++ ', end='', flush=True)

                if m == 'nn':
                    res, attr = clf(X, y, k=1, clf_method='nn')

                elif m == 'dmc':
                    res, attr = clf(X, y, k=2, clf_method='dmc')

                elif m == 'cq':
                    res, attr = clf(X, y, k=1, clf_method='cq')

                elif m == 'lmq':
                    res, attr = clf(X, y, clf_method='lmq')  # não precisa de k para lmq

                else:
                    raise SystemExit('ERRO_1')

                accuracys.append(res)

                attr_values[t, ...] = np.array(attr)

            accuracys_resume[0, :] = [np.mean(accuracys), np.median(accuracys),
                                      np.min(accuracys), np.max(accuracys),
                                      np.std(accuracys)]

            attr_values = np.sum(attr_values, axis=0)

            np.savetxt(f'{m}_attr_values_{size}.txt', attr_values, fmt='%d')

            # para a tabela do TC2
            np.savetxt(f'{m}_accuracy_mean_median_min_max_std_{size}.txt', accuracys_resume, fmt='%.5f')

            # para o boxplot
            np.savetxt(f'{m}_all_accuracys_{size}_nn.txt', np.array(accuracys), fmt='%.5f')

















    # Showing Prediction Accuracy as a function of K
    # https://anujkatiyal.com/blog/2017/10/01/ml-knn/

    # Plot the Decision Boundary of the k-NN Classifier
    # https://towardsdatascience.com/solving-a-simple-classification-problem-with-python-fruits-lovers-edition-d20ab6b071d2

    # boxplot
    # https://machinelearningmastery.com/compare-machine-learning-algorithms-python-scikit-learn/

    # error plot
    # https://stackabuse.com/k-nearest-neighbors-algorithm-in-python-and-scikit-learn/
