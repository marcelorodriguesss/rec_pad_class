import seaborn as sns
import matplotlib.pyplot as plt

def plot_cov(cov_mat):

    plt.figure(figsize=(20, 20))

    sns.set(font_scale=1.5)

    hm = sns.heatmap(cov_mat,
                     cbar=True,
                     annot=True,
                     square=True,
                     fmt='.2f',
                     annot_kws={'size': 12},
                     cmap='coolwarm',
                     yticklabels='',
                     xticklabels='')

    plt.title('MATRIZ DE COVARIÂNCIA', size=25)

    plt.tight_layout()

    # plt.show()

    plt.savefig('cov.png', dpi=300)
