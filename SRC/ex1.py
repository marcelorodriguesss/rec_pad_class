import time
import numpy as np
import streamlit as st
# import matplotlib.pyplot as plt

from plot_cov import plot_cov

# pip3.6 install streamlit

# streamlit run ex1.py

# ref: https://carbon.now.sh

@st.cache
def read_data():
    # substituído g por 1 e b por -1
    iono_file = '../DADOS/IONOSPHERE/ionosphere.data'
    Xori = np.loadtxt(iono_file, delimiter=',')
    X = Xori[:, :-1]  # X sem classes
    return Xori, X


@st.cache
def calc_cov_eq68(X):
    t = time.time()
    # vetor médio eq 64
    m = np.mean(X, axis=0)
    Xlen = X.shape[0]
    # matriz de zeros que será preenchida
    Xnew = np.zeros(X.shape)
    for n in range(X.shape[1]):
        Xnew[:, n] = X[:, n] - m[n]
    Cx = Xnew.T.dot(Xnew) / Xlen
    elapsed = time.time() - t
    return Cx, elapsed


@st.cache
def calc_cov_eq69(X):
    t = time.time()
    # vetor médio eq 64
    m = np.mean(X, axis=0)
    m = m.reshape(1, m.shape[0])
    # correlação eq 65
    Xlen = X.shape[0]
    Rx = (X.T.dot(X)) / Xlen
    Cx = Rx - m * m.T
    elapsed = time.time() - t
    return Cx, elapsed


@st.cache
def calc_cov_eq70(X):
    t = time.time()
    # vetor médio eq 64
    m = np.mean(X, axis=0)
    # repete os valores de m para uma matriz
    m = np.tile(m, (X.shape[0], 1))
    Xlen = X.shape[0]
    xm = X - m
    Cx = xm.T.dot(xm) / Xlen
    elapsed = time.time() - t
    return Cx, elapsed


@st.cache
def calc_media_eq72(X):
    m = np.zeros((1, X.shape[1]))
    for n in range(X.shape[0]):
        i = n + 1
        m = ((i - 1) / i) * m + (1 / i) * X[n, :]
    return m


@st.cache
def calc_correl_eq73(X):
    Rx = np.identity(34)  # 34 x 34
    for n in range(X.shape[0]):
        i = n + 1
        # se não fizer o reshape, Xaux.T * Xaux não
        # estará correto.
        Xaux = X[n, :].reshape(1, 34)  # se não fizer o reshaoe
        Rx = ((i - 1) / i) * Rx + (1 / i) * Xaux.T * Xaux
    return Rx


@st.cache
def calc_cov_eq72_eq73(X):
    t = time.time()
    m = calc_media_eq72(X)
    Rx = calc_correl_eq73(X)
    Cx = Rx - m * m.T
    elapsed = time.time() - t
    return Cx, elapsed


if __name__ == '__main__':

    st.title('Reconhecimento de Padrões')

    Xori, X = read_data()
    st.info(f'Xori = {Xori.shape} :::: X = {X.shape}')

    st.warning(':::: CÁLCULO DAS COVARIÂNCIAS GLOBAL ::::')

    t = time.time()
    cov = np.cov(X.T, bias = True)
    elapsed = time.time() - t
    st.info(f'NUMPY :: {cov.shape} :: {elapsed}s')
    st.write(cov)

    plot_cov(cov)

    cov, elapsed = calc_cov_eq68(X)
    st.info(f'EQ 68 :: {cov.shape} :: {elapsed}s')
    st.write(cov)

    cov, elapsed = calc_cov_eq69(X)
    st.info(f'EQ 69 :: {cov.shape} :: {elapsed}s')
    st.write(cov)

    cov, elapsed = calc_cov_eq70(X)
    st.info(f'EQ 70 :: {cov.shape} :: {elapsed}s')
    st.write(cov)

    cov, elapsed = calc_cov_eq72_eq73(X)
    st.info(f'EQ 72 EQ 73 :: {cov.shape} :: {elapsed}s')
    st.write(cov)

    st.warning(':::: CÁLCULO DAS COVARIÂNCIAS POR CLASSES ::::')

    st.info('CLASSE 1')
    X1 = Xori[np.where(Xori[:, -1] == 1)]  # separa em classes
    X1 = X1[:, :-1]
    cov, elapsed = calc_cov_eq68(X1)
    st.write(cov)

    st.info('CLASSE -1')
    X2 = Xori[np.where(Xori[:, -1] == -1)]  # separa em classes
    X2 = X2[:, :-1]
    cov, elapsed = calc_cov_eq68(X2)
    st.write(cov)

    st.warning(':::: RANKS ::::')

    st.write(f'RANK GLOBAL: {np.linalg.matrix_rank(X)}')
    st.write(f'RANK CLASSE  1: {np.linalg.matrix_rank(X1)}')
    st.write(f'RANK CLASSE -1: {np.linalg.matrix_rank(X2)}')

    # st.write(np.linalg.cond(X))
    # st.write(np.linalg.cond(X1))
    # st.write(np.linalg.cond(X2))

    # st.write(np.linalg.det(X))

    ### TRASHA AREA ###

